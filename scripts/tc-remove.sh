#!/bin/bash

scripts_root="$(dirname $0)"
env_file="$scripts_root/trash.env"

[ -r "$env_file" ] && source "$env_file"

tc_url="${TC_URL:-"http://localhost:8000/"}"
tmpdir="${TC_TMPDIR:-"/tmp/"}"

function usage() {
	echo "$0 -i <paper_id>"
}

paper_id=

while getopts "h?i:q:" opt
do
	case "$opt" in
		h|\?)
			usage
			exit 0
			;;
		i)
			paper_id=$OPTARG
			;;
	esac
done

if [ -z "$paper_id" ]
then
	usage
	exit 1
fi

curl --silent -L -X DELETE "${tc_url}papers/${paper_id}" | jq

