#!/bin/bash

path="$1"
tika="${TIKA_JAR_PATH:-"/usr/local/share/trashcan/tika-app.jar"}"

[ ! -r "$path" ] && exit 1

mime="$(file --mime-type "$path" --brief)"

case "$mime" in
	text/plain*)
		cat "$path"
		;;
	*)
		java -jar $tika --text "$path" 2>/dev/null
		;;
esac

