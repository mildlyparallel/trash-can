#!/bin/bash

scripts_root="$(dirname $0)"
env_file="$scripts_root/trash.env"

[ -r "$env_file" ] && source "$env_file"

tc_url="${TC_URL:-"http://localhost:8000/"}"
tmpdir="${TC_TMPDIR:-"/tmp/"}"

function usage() {
	echo "$0 [-i paper_id] [-q query]"
}

query=
paper_id=

while getopts "h?i:q:" opt
do
	case "$opt" in
		h|\?)
			usage
			exit 0
			;;
		i)
			paper_id=$OPTARG
			;;
		q)
			query=$OPTARG
			;;
	esac
done

function print_output {
	resp="$tmpdir/trash-can-response.csv"
	echo 'ID,Year,Title,First Author' > "$resp"
	cat "$1" | jq -r '.papers[] | [.paper_id, .year, .title[0:60], .first_author[0:10]] | @csv' >> "$resp"
	cat "$resp" | tr -d \" | column -t -s,
}

function find_by_id() {
	resp="$tmpdir/trash-can-response.json"
	> $resp
	curl --silent -L "${tc_url}papers/${paper_id}" -o "$resp"
	cat "$resp" | jq '.papers[0]'
}

function find_with_query() {
	resp="$tmpdir/trash-can-response.json"
	> $resp
	curl --silent -L -F "query=$query" "${tc_url}papers/query" -o "$resp"
	print_output "$resp"
}

function find_with_any() {
	resp="$tmpdir/trash-can-response.json"
	> $resp
	curl --silent -L "${tc_url}papers" -o "$resp"
	print_output "$resp"
}

if [ -n "$paper_id" ]
then
	find_by_id
	exit 0
fi

if [ -n "$query" ]
then
	find_with_query
	exit 0
fi

find_with_any
