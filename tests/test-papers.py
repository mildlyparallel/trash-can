import os
import unittest
import requests
import urllib
import random
import string

from pprint import pprint

from resttest import RestTest

class TestTags(RestTest):
    def test__get__empty__returns_list(self):
        (code, body) = self._get("tags")
        self.assertEqual(code, 200);
        self.assertTrue(type(body) == list);

    def test__post__only_title__returns_id(self):
        (code, body) = self._post("/papers", [
            ('title', 'name1'),
        ])

        self.assertEqual(code, 200);
        self.assertTrue(body['paper_id'] > 0);

    def test__post__empty_title__returns_err(self):
        (code, body) = self._post("/papers", [
            ('title', ''),
        ])

        self.assertEqual(code, 400);

    def test__post__no_title__returns_err(self):
        (code, body) = self._post("/papers", [
            ('k', 'v'),
        ])

        self.assertEqual(code, 400);

    def __post_tag(self):
        tag = {
            'name': self._genstr(),
            'color': self._genstr()
        }

        (code, body) = self._post("/tags", tag)
        self.assertEqual(code, 200);

        tag['tag_id'] = body['tag_id']

        return tag

    def __generate_fields(self):
        paper = {
            'title': self._genstr(),
            'authors': ','.join([self._genstr(), self._genstr(), self._genstr()]),
            'doi': self._genstr(),
            'isbn': self._genstr(),
            'arxiv': self._genstr(),
            'url': self._genstr(),
            'notes': self._genstr(),
            'bibtex': self._genstr(),
            'year': random.randint(1970, 9999) 
        }

        return paper

    def __check_paper_fields(self, body, paper, tags):
        for k, v in paper.items():
            if k == 'tags':
                continue
            self.assertEqual(body['papers'][0][k], paper[k]);

        self.assertIn('created_at', body['papers'][0])
        self.assertIn('updated_at', body['papers'][0])
        self.assertIn('indexed_at', body['papers'][0])

        authors = paper['authors'].split(',')
        self.assertEqual(body['papers'][0]['first_author'], authors[0]);

        if tags:
            self.assertEqual(len(body['tags']), len(tags))
            for tag in body['tags']:
                t = { k: tag[k] for k in ['tag_id', 'name', 'color'] }
                self.assertIn(t, tags)


    def test__create__all_fields(self):
        tags = [
            self.__post_tag(),
            self.__post_tag(),
        ]

        paper = self.__generate_fields()
        paper['tags'] = [ t['tag_id'] for t in tags ]

        (code, body) = self._post("/papers", paper, '/etc/hosts')

        self.assertEqual(code, 200);
        self.assertTrue(body['paper_id'] > 0);

        paper_id = body['paper_id']

        (code, body) = self._get(f'papers/{paper_id}')

        self.assertEqual(code, 200);
        self.__check_paper_fields(body, paper, tags)

        self.assertTrue(body['papers'][0]['filesize'] > 0);
        self.assertEqual(body['papers'][0]['filemime'], 'text/plain');

    def test__update__all_fields(self):
        tags = [
            self.__post_tag(),
            self.__post_tag(),
        ]

        paper = self.__generate_fields()
        paper['tags'] = [ t['tag_id'] for t in tags ]

        (code, body) = self._post("/papers", paper, '/etc/hosts')

        self.assertEqual(code, 200);
        self.assertTrue(body['paper_id'] > 0);

        paper_id = body['paper_id']

        paper = self.__generate_fields()

        (code, body) = self._post(f"/papers/{paper_id}", paper, '/etc/passwd')

        (code, body) = self._get(f'papers/{paper_id}')

        self.assertEqual(code, 200);

        self.__check_paper_fields(body, paper, tags)

        self.assertTrue(body['papers'][0]['filesize'] > 0);
        self.assertEqual(body['papers'][0]['filemime'], 'text/plain');

if __name__ == '__main__':
    unittest.main()

