#include <signal.h>
#include <iostream>
#include <fstream>

#include "Config.hh"
#include "Logger.hh"
#include "TrashServer.hh"
#include "Database.hh"
#include "Magic.hh"
#include "Indexer.hh"
#include "FileStorage.hh"

#include "orm/Tags.hh"
#include "orm/TagLinks.hh"
#include "orm/Papers.hh"
#include "orm/PapersIndex.hh"

void create_tables(Database &db)
{
	Papers::Create papers(db);
	papers.step();

	PapersIndex::Create index(db);
	index.step();

	Tags::Create tags(db);
	tags.step();

	TagLinks::Create links(db);
	links.step();
}

int main(int argc, const char *argv[])
{
	Config config;
	config.parse_cmdline(argc, argv);

	Database db_srv;
	logger.info("Using database", config.database_uri);
	db_srv.open(config.database_uri);

	create_tables(db_srv);

	Magic magic;
	magic.load();

	logger.info("Storing files in", config.storage_dir);
	FileStorage storage(config);

	Database db_idx;
	db_idx.open(config.database_uri);

	Indexer::ignore_sigchld();
	Indexer indexer(config, db_idx);
	indexer.spawn();

	if (!config.static_dir.empty())
		logger.info("Serving assets from", config.static_dir);

	TrashServer server(config, db_srv, magic, indexer, storage);

	TrashServer::set_exit_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT});

	server.run();

	indexer.join();

	return 0;
}

