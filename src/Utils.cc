#include <cctype>
#include <algorithm>
#include <limits>

#include "Utils.hh"

namespace utils {

std::string first_word(const std::string_view &s)
{
	std::string w;

	for (size_t i = 0; i < min(s.size(), 15u); ++i) {
		if (!std::isalpha(s[i]))
			break;

		w += std::tolower(s[i]);
	}

	return w;
}

} /* namespace utils */
