#include <cassert>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <cstdint>

#include <sqlite3.h>

#include "Database.hh"
#include "Logger.hh"

#define CHECK(rc) {\
	if ((rc) != SQLITE_OK) { \
		logger.error("sqlite3 error #", sqlite3_errcode(m_handle), "@ line", __LINE__, ":", sqlite3_errmsg(m_handle)); \
		throw SqliteException(sqlite3_errcode(m_handle)); \
	} \
} while(false);

Database::Database()
: m_handle(nullptr)
{ }

Database::~Database()
{
	if (m_handle)
		sqlite3_close(m_handle);
}

void Database::open(const std::string &uri)
{
	assert(!uri.empty());
	logger.debug("Opening database:", uri);

	assert(!m_handle);

	int rc = sqlite3_open(uri.c_str(), &m_handle);
	CHECK(rc);

	init_pragmas();
}

void Database::exec(const char *sql)
{
	assert(m_handle);
	assert(sql);

	char *err = nullptr;

	int rc = sqlite3_exec(m_handle, sql, nullptr, 0, &err);

	if (rc != SQLITE_OK) {
		logger.error("Failed to execute", sql);
		logger.error("sqlite3 error #", sqlite3_errcode(m_handle),  "#", rc, ":", err);
		sqlite3_free(err);
		throw SqliteException(rc);
	}
}

void Database::init_pragmas()
{
	exec("PRAGMA foreign_keys = ON;");
}

void Database::run_file(const std::string &p)
{
	assert(!p.empty());
	logger.info("Running SQL from", p);

	std::ifstream in(p);
	std::string content(
		(std::istreambuf_iterator<char>(in)),
		std::istreambuf_iterator<char>()
	);

	exec(content.c_str());
}

sqlite3 *Database::get_handle()
{
	return m_handle;
}

uint64_t Database::get_rowid() const
{
	return sqlite3_last_insert_rowid(m_handle);
}

SqliteException::SqliteException(int rc)
: m_code(rc)
{ }

const char *SqliteException::what() const noexcept {
	return sqlite3_errstr(m_code);
}

bool Database::is_threadsafe()
{
	return sqlite3_threadsafe();
}
