#include <filesystem>
#include <stdexcept>

#include <Dashh.hh>

#include "Config.hh"
#include "Logger.hh"
#include "Utils.hh"

void Config::parse_cmdline(int argc, const char *argv[])
{
	using namespace dashh;

	Command cmd;

	cmd.setVersion(VERSION_STR);

	cmd << "trashcan -- Backend service of your trash";

	cmd << "\nUsage:";
	cmd << Usage("[-v] [-u URL] [options...]");

	cmd << "\nOptions:";

	cmd << Option(listen_url, "-u, --listen", "URL",
		"Service listening address (default: " TC_DFT_LISTEN_URL ")"
	);

	cmd << Option(static_dir, "-s, --serve", "DIR",
		"Path for serving static directory (default:" TC_DFT_SERVE_DIR ")"
	);

	cmd << Option(storage_dir, "-p, --storage", "DIR",
		"Path for storing uploaded files (default: " TC_DFT_STORAGE_DIR ")"
	);

	cmd << Option(database_uri, "-d, --database", "URI",
		"Sqlite database uri (default: " TC_DFT_DB_URI ")"
	);

	cmd << Option(content_extract_cmd, "-e, --extract-cmd", "CMD",
		"Command for extracting papers content (default: " TC_CONTENT_EXTRACT_CMD ")"
	);

	std::string log_level;
#ifndef NDEBUG
	cmd << Option(as_state(log_level, {"error", "warning", "info", "verbose", "debug"}), "-L, --log-level",
		"error|warning|info|verbose|debug",
		"Log level (default: info)"
	);
#else
	cmd << Option(as_state(log_level, {"error", "warning", "info", "verbose"}), "-L, --log-level",
		"error|warning|info|verbose",
		"Log level (default: info)"
	);
#endif

	cmd << Option(log_file, "-l, --log", "PATH",
		"Path to the log file (default: stderr)"
	);

	bool log_verbose = false;
	cmd << Flag(log_verbose, "-v, --verbose", "Set verbose log level");

	cmd << Flag(PageHelp(), "-h, --help", "Print help (this message) and exit");
	cmd << Flag(PageVersion(), "--version", "Print version information and exit");

	Dashh t(&cmd);
	t.parse(argc, argv);

	if (log_level == "error")
		logger.set_level(Logger::Error);
	else if (log_level == "warning")
		logger.set_level(Logger::Warning);
	else if (log_level == "verbose")
		logger.set_level(Logger::Warning);
#ifndef NDEBUG
	else if (log_level == "debug")
		logger.set_level(Logger::Debug);
#endif
	else
		logger.set_level(Logger::Info);

	if (log_verbose && log_level != "debug")
		logger.set_level(Logger::Verbose);

	if (!log_file.empty())
		logger.set_output_file(log_file);

	if (storage_dir.empty())
		throw std::runtime_error("Storage directory (-p) is empty");

	std::filesystem::create_directories(storage_dir);
}
