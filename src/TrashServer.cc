#include <string_view>
#include <iostream>
#include <string>
#include <cassert>

#include "TrashServer.hh"

#include "Config.hh"
#include "Logger.hh"
#include "Database.hh"
#include "Transaction.hh"
#include "UserError.hh"
#include "Magic.hh"
#include "Indexer.hh"
#include "FileStorage.hh"
#include "Search.hh"

#include "orm/Tags.hh"
#include "orm/TagLinks.hh"
#include "orm/Papers.hh"
#include "orm/PapersIndex.hh"

extern "C" {
#include <mongoose.h>
}

TrashServer::TrashServer(
	const Config &config,
	Database &database,
	const Magic &magic,
	Indexer &indexer,
	FileStorage &storage
)
: m_config(config)
, m_db(database)
, m_magic(magic)
, m_indexer(indexer)
, m_storage(storage)
{  }

TrashServer::~TrashServer()
{ }

void TrashServer::find_paper(const Request &req)
{
	uint64_t paper_id = get_id(req, 0);

	logger.verbose("Searching paper", paper_id);

	Search papers(m_db);
	papers.find_one(paper_id);

	req.reply(papers.get());
}

void TrashServer::find_papers_latest(const Request &req)
{
	uint64_t limit = req.get_query_ull("limit");
	uint64_t offset = req.get_query_ull("offset");

	logger.verbose("Searching latest papers");

	Search papers(m_db);
	papers.set_limit_offset(limit, offset);
	papers.find_latest();

	req.reply(papers.get());
}

void TrashServer::find_papers(const Request &req)
{
	logger.verbose("Searching papers");

	uint64_t limit = req.get_query_ull("limit");
	uint64_t offset = req.get_query_ull("offset");

	std::set<uint64_t> tags;
	std::string_view query;

	for (const auto &[name, filename, body] : req) {
		if (name == "tags")
			tags.insert(get_id(body));
		else if (name == "query")
			query = body;
	}

	Search papers(m_db);
	papers.set_limit_offset(limit, offset);

	if (tags.empty() && query.empty())
		papers.find_latest();
	else if (tags.empty())
		papers.find_by_query(query);
	else if (query.empty())
		papers.find_by_tags(tags);
	else
		papers.find_by_tags_and_query(tags, query);

	req.reply(papers.get());
}

void TrashServer::get_paper_file(const Request &req)
{
	uint64_t paper_id = get_id(req, 1);

	logger.verbose("Serving file for paper", paper_id);

	Papers::SelecFiledata paper(m_db);
	paper.bind(paper_id);

	if (!paper.load())
		throw UserError(UserError::FileNotExists);

	if (paper.filesize == 0 || paper.filename.empty())
		throw UserError(UserError::FileNotExists);

	std::string headers = "Content-disposition: inline; filename=\"";
	headers += paper.filename;
	headers += "\"\r\n";

	req.serve_file(m_storage.get_path(paper_id), paper.filemime, headers);
}

void TrashServer::get_paper_bibtex(const Request &req)
{
	logger.verbose("Searching bibtex");

	std::stringstream qss;
	qss << Papers::SelectBibtex::sql_part_1;

	bool have_some = false;

	for (const auto &[name, file, body] : req) {
		if (name != Papers::name(Papers::PaperId))
			continue;

		if (have_some)
			qss << ',';
		qss << get_id(body);
		have_some = true;
	}

	if (!have_some)
		throw UserError(UserError::MissingFields);

	qss << Papers::SelectBibtex::sql_part_2;

	Serializer body(Config::resp_buf_size_start_med);

	Papers::SelectBibtex bibtex(m_db, qss.str().c_str());
	bibtex.write_all(body);

	req.reply(body.get());
}

void TrashServer::update_paper_file(const Request &req)
{
	uint64_t paper_id = get_id(req, 1);

	logger.verbose("Updating file for paper", paper_id);

	auto [filename, filedata] = req.get_first_file();

	if (filename.empty() || filedata.empty())
		throw UserError(UserError::MissingFields);

	Transaction transaction(m_db);

	Papers::UpdateFile file(m_db);
	file.paper_id = paper_id;
	file.filename = filename;
	file.filesize = filedata.size();
	file.filemime = m_magic.mime(filedata);
	file.run();

	m_storage.add(paper_id, filedata);
	m_indexer.push(paper_id);

	transaction.commit();

	req.reply();
}

void TrashServer::update_paper_index(const Request &req)
{
	uint64_t paper_id = get_id(req, 1);

	logger.verbose("Pushing paper", paper_id, "to indexer");

	m_indexer.push(paper_id);

	req.reply();
}

void TrashServer::add_paper_tags(const Request &req)
{
	uint64_t paper_id = get_id(req, 1);

	logger.verbose("Tagging paper", paper_id);

	Serializer response;
	response.push_list();

	Transaction transaction(m_db);

	bool inserted = false;

	for (const auto &[name, file, body] : req) {
		if (name != "tags")
			continue;

		TagLinks::Insert link(m_db);
		link.paper_id = paper_id;
		link.tag_id = get_id(body);
		link.run();

		link.write(response);
		inserted = true;
	}

	response.pop();

	if (!inserted)
		throw UserError(UserError::MissingFields);

	transaction.commit();

	req.reply(response.get());
}

void TrashServer::create_paper(const Request &req)
{
	std::string_view filedata;

	logger.verbose("Creating new paper");

	Papers::Insert paper(m_db);
	PapersIndex::Insert index(m_db);

	for (const auto &[name, file, body] : req) {
		if (!file.empty() && !body.empty()) {
			paper.filename = file;
			filedata = body;
		} else if (name == Papers::names[Papers::Title]) {
			paper.title = body;
			index.title = body;
		} else if (name == Papers::names[Papers::Authors]) {
			paper.authors = body;
			index.authors = body;

			size_t comma_pos = body.find(',');
			if (comma_pos == body.npos)
				paper.first_author = body;
			else
				paper.first_author = body.substr(0, comma_pos);
		} else if (name == Papers::names[Papers::Doi]) {
			paper.doi = body;
			index.doi = body;
		} else if (name == Papers::names[Papers::Isbn]) {
			paper.isbn = body;
			index.isbn = body;
		} else if (name == Papers::names[Papers::Arxiv]) {
			paper.arxiv = body;
			index.arxiv = body;
		} else if (name == Papers::names[Papers::Bibtex]) {
			paper.bibtex = body;
		} else if (name == Papers::names[Papers::Url]) {
			paper.url = body;
		} else if (name == Papers::names[Papers::Notes]) {
			paper.notes = body;
			index.notes = body;
		} else if (name == Papers::names[Papers::Year]) {
			try {
				paper.year = std::stoull(std::string(body));
				index.year = body;
			} catch (...) { };
		}
	}

	if (!filedata.empty()) {
		paper.filesize = filedata.size();
		paper.filemime = m_magic.mime(filedata);
	}

	Transaction transaction(m_db);

	paper.run();

	index.paper_id = paper.paper_id;

	index.run();

	for (const auto &[name, file, body] : req) {
		if (name != "tags")
			continue;

		TagLinks::Insert link(m_db);
		link.paper_id = paper.paper_id;
		link.tag_id = get_id(body);
		link.run();
	}

	if (!filedata.empty()) {
		m_storage.add(paper.paper_id, filedata);
		m_indexer.push(paper.paper_id);
	}

	transaction.commit();

	logger.verbose("New paper id", paper.paper_id);

	Serializer body;
	paper.write(body);
	req.reply(body.get());
}

void TrashServer::update_paper(const Request &req)
{
	std::string_view filedata;

	Papers::Update paper(m_db);
	PapersIndex::Update index(m_db);

	paper.paper_id = get_id(req, 0);
	index.paper_id = paper.paper_id;

	logger.verbose("Updating paper", paper.paper_id);

	for (const auto &[name, file, body] : req) {
		if (name == Papers::names[Papers::Title]) {
			paper.title = body;
			index.title = body;
		} else if (name == Papers::names[Papers::Authors]) {
			paper.authors = body;
			index.authors = body;

			size_t comma_pos = body.find(',');
			if (comma_pos == body.npos)
				paper.first_author = body;
			else
				paper.first_author = body.substr(0, comma_pos);

		} else if (name == Papers::names[Papers::Doi]) {
			paper.doi = body;
			index.doi = body;
		} else if (name == Papers::names[Papers::Isbn]) {
			paper.isbn = body;
			index.isbn = body;
		} else if (name == Papers::names[Papers::Bibtex]) {
			paper.bibtex = body;
		} else if (name == Papers::names[Papers::Arxiv]) {
			paper.arxiv = body;
			index.arxiv = body;
		} else if (name == Papers::names[Papers::Bibtex]) {
			paper.bibtex = body;
		} else if (name == Papers::names[Papers::Url]) {
			paper.url = body;
		} else if (name == Papers::names[Papers::Notes]) {
			paper.notes = body;
			index.notes = body;
		} else if (name == Papers::names[Papers::Year]) {
			try {
				paper.year = std::stoull(std::string(body));
				index.year = body;
			} catch (...) { };
		}
	}

	Transaction transaction(m_db);

	paper.run();
	index.run();

	transaction.commit();

	Serializer body;
	paper.write(body);
	req.reply(body.get());
}

void TrashServer::delete_paper(const Request &req)
{
	uint64_t paper_id = get_id(req, 0);

	logger.verbose("Deleting paper", paper_id);

	Transaction transaction(m_db);

	TagLinks::DeleteByPaper links(m_db);
	links.id = paper_id;
	links.run();

	Papers::Delete paper(m_db);
	paper.paper_id = paper_id;
	paper.run();

	PapersIndex::Delete index(m_db);
	index.paper_id = paper_id;
	index.run();

	m_storage.remove(paper_id);

	transaction.commit();

	req.reply();
}

void TrashServer::find_tags_all(const Request &req)
{
	Tags::Select tag(m_db);

	logger.verbose("Listing all tag");

	Serializer body(Config::resp_buf_size_start_med);

	body.push_list();

	while (tag.run())
		tag.write(body);

	body.pop();

	req.reply(body.get());
}

void TrashServer::create_tag(const Request &req)
{
	Tags::Insert tag(m_db);

	logger.verbose("Creating new tag");

	for (const auto &[name, filename, body] : req) {
		if (name == Tags::names[Tags::Name])
			tag.name = body;
		else if (name == Tags::names[Tags::Color])
			tag.color = body;
	}

	tag.run();

	logger.debug("New tag id:", tag.tag_id);

	Serializer body;
	tag.write(body);

	req.reply(body.get());
}

void TrashServer::remove_tag(const Request &req)
{
	uint64_t tag_id = get_id(req, 0);

	Transaction transaction(m_db);

	logger.verbose("Deleting tag", tag_id, " and its links");

	TagLinks::DeleteByTag link(m_db);
	link.id = tag_id;
	link.run();

	Tags::Delete tag(m_db);
	tag.tag_id = tag_id;
	tag.run();

	transaction.commit();

	req.reply();
}

void TrashServer::unlink_tag(const Request &req)
{
	uint64_t link_id = get_id(req, 0);

	logger.verbose("Deleting link", link_id);

	TagLinks::Delete link(m_db);
	link.id = link_id;
	link.run();

	req.reply();
}

uint64_t TrashServer::get_id(const std::string &s) {
	uint64_t id = 0;

	try {
		id = std::stoull(s);
	} catch (...) { };

	if (id == 0)
		throw UserError(UserError::InvalidId);

	return id;
}

void TrashServer::dispatch(const Request &req)
{
	if (req.method() == Request::Options) {
		req.reply("");
		return;
	}

	for (const auto &route : routes) {
		if (req.method() != route.method)
			continue;
		if (!req.match_uri(route.uri))
			continue;

		logger.debug("Matched", route.method, route.uri);

		(this->*(route.func))(req);
		return;
	}

	if (!m_config.static_dir.empty())
		req.serve_dir(m_config.static_dir.c_str());
}

void TrashServer::run()
{
	listen(m_config.listen_url);

	poll_loop(m_config.poll_delay_ms);
}
