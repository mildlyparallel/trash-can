import axios from 'axios';

const getDoiUrl = function(str) {
	if (!str)
		return null;

	str = decodeURIComponent(str.trim());

	try {
		var url = new URL(str);

		if (url.host == 'doi.org')
			return str;

		return null;
	} catch (e) {
		return 'https://doi.org/' + str;
	}
};

const fetchMetadata = async function(url) {
	var res = await axios({
		method: "GET",
		url: url,
		headers: {
			"Accept": "application/json"
		}
	});

	if (res.status != 200)
		return null;

	var raw = res.data;

	var data = {
		doi: url,
		url: url
	};

	if ('title' in raw)
		data['title'] = raw.title;

	if ('author' in raw) {
		var authors = [];

		for (let i = 0; i < raw.author.length; i++) {
			var name = '';
			if ('family' in raw.author[i])
				name += raw.author[i].family + ' ';
			if ('given' in raw.author[i])
				name += raw.author[i].given;
			if (name == '')
				continue;

			authors.push(name)

			var first = 'sequence' in raw.author[i] && raw.author[i].sequence == 'first';

			if (first || !data['first_author']) {
				data['first_author'] = name;
			}
		}

		data['authors'] = authors.join(', ');
	}

	if ('created' in raw && 'timestamp' in raw.created) {
		var d = new Date(raw.created.timestamp);
		data['year'] = d.getFullYear();
	}

	return data;
}

const fetchBibtex = async function(url) {
	var res = await axios({
		method: "GET",
		url: url,
		headers: {
			"Accept": "text/bibliography; style=bibtex"
		}
	});

	if (res.status != 200)
		return null;

	return res.data;
}

const Doi = {
	fetch: async function(doistr) {
		var url = getDoiUrl(doistr);

		if (!url)
			throw 'Failed to parse doi ' + doistr;

		var [metadata, bibtex] = await Promise.all([
			fetchMetadata(url),
			fetchBibtex(url)
		])

		if (!metadata)
			return null;

		if (bibtex)
			metadata['bibtex'] = bibtex;

		return metadata;
	},
};

export { Doi };

