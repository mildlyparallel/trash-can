import axios from 'axios';

const url = require('url');

const Backend = {
	getBackendUrl: function() {
		var url = localStorage.getItem('backendUrl');
		if (!url)
			url = process.env.VUE_APP_BACKEND_URL;
		if (!url)
			url = '/';
		return url;
	},

	resolve: function(path) {
		return url.resolve(this.getBackendUrl(), path);
	},

	call: function(method, url, data, params) {
		console.debug(method, url, params, data);

		return new Promise((resolve, reject) => {
			var formdata = new FormData();

			if (data) {
				for (const d of data) {
					var k = Object.keys(d)[0];
					var v = d[k];
					formdata.append(k, v);
				}
			}

			axios({
				method: method,
				url: this.resolve(url),
				data: formdata,
				params: params,
			}).then((res) => {
				if (res.status == 200) {
					console.debug('reply:', res.status);
					resolve(res.data);
				} else {
					console.error('reply:', res);
					reject(res);
				}
			}).catch((error) => {
				console.error('call error:', error);
				reject(error);
			});
		});

	},

	get: function(url, params) {
		return this.call('GET', url, null, params);
	},

	post: function(url, data, params) {
		return this.call('POST', url, data, params);
	},

	delete: function(url, params) {
		return this.call('DELETE', url, null, params);
	},
};

export { Backend };

