import {Backend} from '../api/Backend';

const paperFields = [
	'title',
	'authors',
	'year',
	'year',
	'first_year',
	'doi',
	'isbn',
	'arxiv',
	'bibtex',
	'notes',
	'url'
]

function fieldsToData(paper) {
	var data = [];

	for (let f of paperFields) {
		if (!paper[f])
			continue;

		var d = {};
		d[f] = paper[f];
		data.push(d);
	}

	return data;
}

const Papers = {
	getPapersList: function(response) {
		var papers = [];

		for (var paper of response.papers) {
			paper.tags = [];

			for (const tag of response.tags) {
				if (tag.paper_id == paper.paper_id)
					paper.tags.push(tag);
			}

			paper.has_file = paper.filesize > 0;

			papers.push(paper);
		}

		return papers;
	},

	findOne: async function(paper_id) {
		var reponse = await Backend.get('papers/' + paper_id);
		var p = this.getPapersList(reponse);
		if (p.length != 1)
			return null;

		return p[0];
	},

	update: async function(paper) {
		var data = fieldsToData(paper);

		await Backend.post('papers/' + paper.paper_id, data, {});
	},

	create: async function(paper, file) {
		var data = fieldsToData(paper);

		if (file)
			data.push({file: file});

		var resp = await Backend.post('papers', data, {});
		return resp.paper_id;
	},

	upload: async function(paper_id, file) {
		var data = [
			{file: file}
		];

		await Backend.post('papers/' + paper_id + '/file', data, {});
	},

	find: async function(limit, offset, query, tagsIds) {
		var data = [ ];

		if (query)
			data.push({query: query});

		for (var tagId of tagsIds)
			data.push({tags: tagId});

		var params = {
			limit: limit,
			offset: offset
		};

		var resp = await Backend.post('papers/query', data, params);
		return this.getPapersList(resp);
	},

	getFilePath: function(paper_id) {
		return Backend.resolve(`papers/${paper_id}/file`);
	},

	link: async function(paper_id, tags) {
		var data = [ ];

		for (const t of tags) {
			data.push({tags: t});
		}

		return await Backend.post(`papers/${paper_id}/tags`, data, {});
	},

	unlink: async function(link_id) {
		await Backend.delete(`link/${link_id}`, [], {});
	},

	index: async function(paper_id) {
		await Backend.post(`papers/${paper_id}/index`, [], {});
	},

	remove: async function(paper_id) {
		await Backend.delete(`papers/${paper_id}`, [], {});
	},

	bibtex: async function(ids) {
		var data = [];

		for (const i of ids)
			data.push({paper_id: i});

		return await Backend.post('papers/bibtex', data, {});
	},
};

export { Papers };

