
import {Tags} from '../api/Tags';

const TagsModule = {
	namespaced: true,

	state: () => ({
		tags: [],
	}),

	mutations: {
		add: function(state, tag) {
			state.tags.push(tag);
		},

		set: function(state, tags) {
			state.tags = tags;
		},

		remove: function(state, tag_id) {
			for (var i in state.tags) {
				if (state.tags[i].tag_id != tag_id)
					continue;

				state.tags.splice(i, 1);
				return;
			}
		}
	},

	actions: {
		add: async function(context, tag) {
			tag.tag_id = await Tags.add(tag.name, tag.color);

			context.commit('add', tag);
		},

		remove: async function(context, tag_id) {
			await Tags.remove(tag_id);

			context.commit('remove', tag_id);
		},

		fetch: async function(context) {
			if (context.state.tags && context.state.tags.length > 0)
				return;

			var tags = await Tags.find();

			context.commit('set', tags);
		}
	},

	getters: { 
		name: state => tag_id => {
			for (var i in state.tags) {
				if (state.tags[i].tag_id == tag_id)
					return state.tags[i].name;
			}

			return '';
		},

		tag: state => tag_id => {
			for (var i in state.tags) {
				if (state.tags[i].tag_id == tag_id)
					return state.tags[i];
			}

			return null;
		}
	}
};

export default TagsModule

