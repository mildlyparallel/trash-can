#pragma once

#include <string>
#include <tuple>
#include <string_view>
#include <cstdint>

class Database;

struct sqlite3;
struct sqlite3_stmt;

class Statement
{
public:
	static const constexpr int busy_retry_delay_ms = 10;

	Statement(Database &db, const char *sql);

	Statement(const Statement &other) = delete;

	Statement(Statement &&other);

	void operator= (Statement &&other);

	virtual ~Statement();

	const char *name_at(size_t pos) const;

	const char *type_at(size_t pos) const;

	size_t nr_columns() const;

	virtual bool load();

	bool step();

protected:
	void bind_to(size_t pos, const char *value);

	void bind_to(size_t pos, const std::string &value);

	void bind_to(size_t pos, const std::string_view &value);

	void bind_to(size_t pos, uint64_t value);

	void load_at(size_t pos, uint64_t &value);

	void load_at(size_t pos, std::string &value);

	void load_at(size_t pos, double &value);

private:
	sqlite3_stmt *m_stmt;

	sqlite3 *m_handle;
};


