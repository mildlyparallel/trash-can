#pragma once

#include <cassert>
#include <utility>
#include <cstddef>

#include "Statement.hh"
#include "Tags.hh"

class TagLinks
{
public:
	enum Field {
		LinkId = 0,
		TagId,
		PaperId,

		nr_fields
	};

	static const constexpr char *names[nr_fields] = {
		"link_id",
		"tag_id",
		"paper_id"
	};

	template <typename T>
	using Fields = std::array<T, nr_fields>;

	inline static const char *name(size_t i) {
		assert(i < nr_fields);
		return names[i];
	}

	inline static size_t size() {
		return nr_fields;
	}

	template <typename T>
	inline static std::pair<const char *, T> make_pair(Field f, const T &value) {
		return std::make_pair(name(f), value);
	}

	struct Create : public Statement {
		static const constexpr char *sql =
			"CREATE TABLE IF NOT EXISTS TagLinks ( "
				"paper_id REFERENCES Papers(paper_id), "
				"tag_id REFERENCES Tags(tag_id) "
			");";

		Create(Database &db)
		: Statement(db, sql)
		{ }
	};

	struct Insert : public Statement {
		static const constexpr char *sql =
			"INSERT INTO TagLinks (paper_id, tag_id) VALUES (?, ?);";

		Insert(Database &db)
		: Statement(db, sql)
		, m_db(db)
		{ }

		uint64_t paper_id = 0;
		uint64_t tag_id = 0;
		uint64_t link_id = 0;

		inline void run() {
			assert(paper_id > 0);
			assert(tag_id > 0);

			bind_to(0, paper_id);
			bind_to(1, tag_id);

			step();

			link_id = m_db.get_rowid();
		}

		inline void write(Serializer &s) {
			assert(link_id > 0);
			assert(tag_id > 0);

			s.write_object(
				make_pair(TagId, tag_id),
				make_pair(LinkId, link_id)
			);
		}

	private:
		Database &m_db;
	};

	struct DeleteById : public Statement {
		DeleteById(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t id = 0;

		inline void run() {
			assert(id > 0);
			bind_to(0, id);
			step();
		}

		inline void write(Serializer &s) {
			s.write_object();
		}
	};

	struct Delete : public DeleteById {
		static const constexpr char *sql =
			"DELETE FROM TagLinks WHERE rowid = ?;";

		Delete(Database &db)
		: DeleteById(db, sql)
		{ }
	};

	struct DeleteByTag : public DeleteById {
		static const constexpr char *sql =
			"DELETE FROM TagLinks WHERE tag_id = ?;";

		DeleteByTag(Database &db)
		: DeleteById(db, sql)
		{ }
	};

	struct DeleteByPaper : public DeleteById {
		static const constexpr char *sql =
			"DELETE FROM TagLinks WHERE paper_id = ?;";

		DeleteByPaper(Database &db)
		: DeleteById(db, sql)
		{ }
	};


	struct SelectTags : public Statement {
		SelectTags(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t link_id;
		uint64_t paper_id;
		uint64_t tag_id;
		std::string name;
		std::string color;

		inline bool run() {
			if (!step())
				return false;

			load_at(0, link_id);
			load_at(1, paper_id);
			load_at(2, tag_id);
			load_at(3, name);
			load_at(4, color);

			return true;
		}

		inline void write_all(Serializer &s) {
			s.push_list();

			while (run())
				write(s);

			s.pop();
		}

		inline void write(Serializer &s) {
			s.write_object(
				make_pair(LinkId, link_id),
				make_pair(PaperId, paper_id),
				make_pair(TagId, tag_id),
				Tags::make_pair(Tags::Name, name),
				Tags::make_pair(Tags::Color, color)
			);
		}
	};

	struct SelectOneTag : public SelectTags {
		static const constexpr char *sql =
			"SELECT TagLinks.rowid, TagLinks.paper_id, TagLinks.tag_id, Tags.name, Tags.color "
			"FROM TagLinks INNER JOIN Tags "
			"ON Tags.tag_id = TagLinks.tag_id AND TagLinks.paper_id = ?;";

		SelectOneTag(Database &db)
		: SelectTags(db, sql)
		{ }

		inline void bind() {
			bind_to(0, paper_id);
		}
	};

	struct SelectAllTags : public SelectTags {
		static const constexpr char *sql_part_1 =
			"SELECT TagLinks.rowid, TagLinks.paper_id, TagLinks.tag_id, Tags.name, Tags.color "
			"FROM TagLinks INNER JOIN Tags "
			"ON Tags.tag_id = TagLinks.tag_id AND TagLinks.paper_id IN (";
		static const constexpr char *sql_part_2 = ");";

		SelectAllTags(Database &db, const char *sql)
		: SelectTags(db, sql)
		{ }
	};
};

