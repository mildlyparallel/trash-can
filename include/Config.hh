#pragma once

#include <string>

#include "config.h"
#include <cstdint>

class Config
{
public:
	std::string listen_url = TC_DFT_LISTEN_URL;

	std::string static_dir = TC_DFT_SERVE_DIR;

	std::string storage_dir = TC_DFT_STORAGE_DIR;

	std::string database_uri = TC_DFT_DB_URI;

	static const constexpr int poll_delay_ms = TC_POLL_INTERVAL_MS;

	static const constexpr int url_param_buf_size = TC_URL_PARAM_BUF_SIZE;

	static const constexpr uint64_t papers_select_limit = TC_DB_ROWS_LIMIT;

	static const constexpr int resp_buf_size_start_small = TC_RESP_BUF_SIZE_SMALL;

	static const constexpr int resp_buf_size_start_med = TC_RESP_BUF_SIZE_MED;

	static const constexpr int resp_buf_size_start_large = TC_RESP_BUF_SIZE_LARGE;

	std::string log_file;

	std::string content_extract_cmd = TC_CONTENT_EXTRACT_CMD;

	void parse_cmdline(int argc, const char *argv[]);

protected:

private:

};
